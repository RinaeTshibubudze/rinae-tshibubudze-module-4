import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'login.dart';

void main() {
  runApp(const Mod4Ass());
}

class Mod4Ass extends StatelessWidget {
  const Mod4Ass({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
          splash: Image.asset(
            'assets/vharhee_logo.png',
          ),
          nextScreen: const LoginPage(),
          splashTransition: SplashTransition.scaleTransition,
          backgroundColor: Colors.deepOrangeAccent,
        ),
        theme: ThemeData(
            primarySwatch: Colors.deepOrange,
            accentColor: /*  */ Colors.deepOrange,
            scaffoldBackgroundColor: Colors.deepOrange));
  }
}
